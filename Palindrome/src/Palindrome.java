
public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("is anna a palindrome? " + Palindrome.isPalindrome("anna"));
	}

	public static boolean isPalindrome(String input) {
		input = input.toUpperCase().replaceAll(" ", "");
		
		for (int i = 0, j = input.length() - 1; i < j ; i++, j-- ) {
			if (input.charAt(i) != input.charAt(j)) {
				return false;
			}
		} 
		return true;
	}
}
